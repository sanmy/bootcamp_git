package Day2;

public class Operators {
	int a,b,c,d,e,f;
	boolean x, y, z;
	
	public static void main(String[] args) {
		Operators op = new Operators();
		//op.arithmeticOperators();
		op.booleanOperators();
		op.terneraryOperators();

	}
	
	public void arithmeticOperators(){
		a = 10; b = 20; c =30; d =40;
		System.out.println("value of a " + a);
		System.out.println("value of a+b " + (a+b));
		System.out.println("value of a-b " + (a-b));
		System.out.println("value of a/b " + (a/b));
		System.out.println("value of a%b " + (a%b));
		System.out.println("value of a*b " + (a*b));
		
		a++;
		++a;
		++a;
		System.out.println("value of a " + a++);
		System.out.println("value of a " + a++);
		System.out.println("value of a " + a--);
		System.out.println("value of a " + --a);
		System.out.println("value of a " + ++a);
		System.out.println("value of a " + ++a);
		System.out.println("value of a " + ++a);
		System.out.println("value of a " + a++);
		System.out.println("value of a " + ++a);
		System.out.println("value of a " + a++);
		
		a+=1;
				System.out.println("value of a " + a);	//20
				System.out.println("value of a+=a " + (a+=a)); //40
				
		e = (a+b) *((a+c)-(b+c) / a);
		System.out.println("e is equal to: " + e);
	}
	public void booleanOperators(){
		x = true; y = false; 
		System.out.println("value of x is: " + x);
		System.out.println("value of y is: " + y);
		System.out.println("value of z is: " + z);
		System.out.println("value of x & y is: " + (x&y));
		System.out.println("value of x | y is: " + (x|y));
		System.out.println("value of x && y is: " + (x&&y));
		System.out.println("value of x || y is: " + (x||y));
		System.out.println("value of ! (x & y) is: " + !(x&y));
		System.out.println("value of ! (x | y) is: " + !(x|y));
		
		//shift operators
		d = 4;	//0000 0100
		f = 5;	//0000 0101
		
		System.out.println("value of d >> f is: " + (d >> 1));	//2
		System.out.println("value of d << f is: " + (d << 1));	//8
		
		System.out.println("value of d << f is: " + (d >> f));	//0
		System.out.println("value of d << f is: " + (d << f));	//128
		
		System.out.println("value of f >> 1 is: " + (f >> 1));	//2
		System.out.println("value of f << 1 is: " + (f << 1));	//10
	}
	
	public void terneraryOperators(){
		a = 10; b = 20;
		int g;
		g = a > 5 ? a : b;
		System.out.println("value of g is: " + g);
		g = a < 10 ? a : b;
		System.out.println("value of g is: " + g);
		g = a > 10 ? a : b;
		System.out.println("value of g is: " + g);
		g = a >= 10 ? a : b;
		System.out.println("value of g is: " + g);
	}

}
