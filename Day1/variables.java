package com.revature.day1;

public class variables {
	int i,j;
	
	public static void main(String[] args) {
		variables mv = new variables();
		mv.myVariables();
		System.out.println("value of i is: " + mv.i);
		System.out.println("value of j is: " + mv.j);
		
		
	}

	public void myVariables(){
		int x,y =20;
		x = 30;
		int z = 10;
		System.out.println("value of x is: " + x);
		System.out.println("value of y is: " + y);
		System.out.println("value of z is: " + z);
		
		System.out.println("=====Swapping variables using a temp variable====");	
		//swap values between x and y. Use z as temp variable.
		z = x;
		x = y;
		y = z;
		System.out.println("value of x is: " + x);
		System.out.println("value of y is: " + y);
		System.out.println("value of z is: " + z);
		
		//swapping variables without using temp variables
		x = x+y;
		y = x-y;
		x = x-y;
	}
}
