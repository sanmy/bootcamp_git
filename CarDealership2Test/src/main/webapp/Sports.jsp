<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sports Cars</title>
</head>
<body>
	<%
		if (session.getAttribute("username") == null) {
			response.sendRedirect("login.jsp");
		}
	%>
	<h1>Delete inventory:</h1>
	<form action='DeleteVehicle' method='get'>
		Delete by Vehicle ID: <input type='text' name='DeleteVehicleDB' />
		<input type='submit' value='Confirm Deletion' />
	</form>
	<form action='Logout'>
		<input type="submit" value='Logout'>

	</form>

</body>
</html>