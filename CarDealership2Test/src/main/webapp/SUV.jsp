<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SUV</title>
</head>
<body>
	<%
		if (session.getAttribute("username") == null) {
			response.sendRedirect("login.jsp");
		}
	%>
	<h1>Update a Vehicle</h1>

	<form action='UpdateVehicle' method='get'>
		Insert the details about the car: <br> Vehicle ID <input
			type='text' name='VehicleIDDB' /><br> Car Make <input
			type='text' name='VehicleMakeDB' /><br> Car Color<input
			type='text' name='VehicleColorDB' /><Br> Car Type<input
			type='text' name='VehicleTypeDB' /><br> <input type='submit'
			value='Confirm Changes'>
	</form>
<br><br>
	<form action='Logout'>
		<input type="submit" value='Logout'>

	</form>

</body>
</html>