package revature.cardealership.jdbc;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
//import java.sql.*;

public class JDBCConnection {
	static Connection c;
	static Statement stmt;
	static ResultSet rs;

	public static void main(String[] args) throws SQLException {

		JDBCConnection JE = new JDBCConnection();
		JE.loadDrivers();
		JE.establishConnection();
		JE.sqlQuery();

		// rs.close();
		// stmt.close();
		// c.close();

		// JE.closeAll();
	}

	void loadDrivers() {
		try {
			// loading the driver
			Class.forName("oracle.jdbc.OracleDriver");
			System.out.println("Drivers are Loaded");
		} catch (ClassNotFoundException e) {
			System.out.println("Drivers did not load");
			e.printStackTrace();
		}
	}

	void establishConnection() {

		// establish the connection
		try {
			c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "sellcars", "vroomvroom");
			System.out.println("The connection is established");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection is Definitely not established");
		}
	}

	void closeAll() throws NullPointerException {

		try {

			rs.close();
			stmt.close();
			c.close();
			System.out.println("I closed my connection");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	void sqlQuery() {
		try {
			System.out.println("Selecting a Table");
			stmt = c.createStatement();

			String sql = "Select * From testing1";

			stmt.executeUpdate(sql);
			System.out.println("Created table in given database...");
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					c.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (c != null)
					c.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try

		// Statement stmt = c.createStatement(ResultSet.TYPE_FORWARD_ONLY,
		// ResultSet.CONCUR_READ_ONLY);
		// ResultSet rs = stmt.executeQuery("select * from test");
		// while (rs.next()) {
		// int id = rs.getInt("col1");
		// int id2 = rs.getInt("col2");
		// int id3 = rs.getInt("col3");
		// System.out.println("the results are" + id + id2 + id3);
		// System.out.println("SQL Query is Complete");
		// }
		// } catch (SQLException e) {
		// System.out.println("SQL Query is Not Complete");
		// }
	}

}
