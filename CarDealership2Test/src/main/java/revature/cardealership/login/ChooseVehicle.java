package revature.cardealership.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ChooseVehicle")
public class ChooseVehicle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ChooseVehicle() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String chooseAction = request.getParameter("chooseAction");

		// String.equals("Parameter");
		if (chooseAction.equals("AddVehicle")) {
			response.sendRedirect("Car.jsp");
		} else if (chooseAction.equals("UpdateVehicle")) {
			response.sendRedirect("SUV.jsp");
		} else if (chooseAction.equals("DeleteVehicle")) {
			response.sendRedirect("Sports.jsp");
		}

	}
}
