package revature.cardealership.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Connection c;
	static Statement stmt;
	static ResultSet rs;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head><title>All Employees</title></head>");
		out.println("<body>");
		out.println("<center><h1>All Employees</h1>");
		
		try {
			c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "sellcars", "vroomvroom");
			System.out.println("The connection is established");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection is Definitely not established");
		}
		
		
		try {
			stmt = c.createStatement();
			String orderBy = request.getParameter("sort");
			if ((orderBy == null) || orderBy.equals("")) {
				orderBy = "SSN";
			}
			String orderByDir = request.getParameter("sortdir");
			if ((orderByDir == null) || orderByDir.equals("")) {
				orderByDir = "asc";
			}
			String query = "SELECT * FROM UserRegistration";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				String uname = rs.getString("Username");
				String pword = rs.getString("Password");
				String pword2 = rs.getString("PasswordConfirmation");
				String email = rs.getString("Email");
				out.print(uname + "::");
				out.print(pword + "::");
				out.print(pword2 + "::");
				out.print(email + "::");
			}
		} catch (SQLException e) {
			out.println("An error occured while retrieving " + "all employees: " + e.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (SQLException ex) {
			}
		}
		out.println("</center>");
		out.println("</body>");
		out.println("</html>");
		out.close();
	}
}
