package revature.cardealership.sql;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet("/AddVehicleDB")
public class AddVehicle extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:oracle:thin:@localhost:1521:xe";
	private static final String DB_USER = "sellcars";
	private static final String DB_PASSWORD = "vroomvroom";

	public AddVehicle() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection dbConnection = null;

		try {

			Class.forName(DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());

		}

		try {

			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		}

		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO VEHICLES2"
				+ "(VehicleID, VehicleType, VehicleMake, VehicleModel, VehicleColor) VALUES" + "(?,?,?,?,?)";

		try {
			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			String VehicleID = request.getParameter("VehicleID");
			String carType = request.getParameter("carType");
			String carMake = request.getParameter("carMake");
			String carColor = request.getParameter("carColor");

			if (carMake.equals("Honda") && carColor.equals("Red") && carType.equals("Sedan")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);

			} else if (carMake.equals("Honda") && carColor.equals("Green") && carType.equals("Sedan")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Blue") && carType.equals("Sedan")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Red") && carType.equals("SUV")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Green") && carType.equals("SUV")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Blue") && carType.equals("SUV")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Red") && carType.equals("Sports")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Green") && carType.equals("Sports")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Blue") && carType.equals("Sports")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Red") && carType.equals("Convertible")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Green") && carType.equals("Convertible")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			} else if (carMake.equals("Honda") && carColor.equals("Blue") && carType.equals("Convertible")) {
				preparedStatement.setString(1, VehicleID);
				preparedStatement.setString(2, carType);
				preparedStatement.setString(3, carMake);
				preparedStatement.setString(4, "null");
				preparedStatement.setString(5, carColor);
			}
			preparedStatement.executeUpdate();
		} catch (

		SQLException e1) {
			e1.printStackTrace();
		}

		System.out.println("Record is inserted into Vehicles2 table!");

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (dbConnection != null) {
			try {
				dbConnection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
